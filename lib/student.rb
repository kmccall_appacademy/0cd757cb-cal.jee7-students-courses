class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    @first_name + " " + @last_name
  end

  def enroll(course)
    unless @courses.include?(course)
      has_conflict?(course)
      @courses << course
      course.students << self
    end
  end

  def has_conflict?(course)
    @courses.each do |enrolled_course|
      if enrolled_course.conflicts_with?(course)
        raise "There is a scheduling conflict."
      end
    end
  end

  def course_load
    credits_per_dpt = Hash.new(0)
    @courses.each do |course|
      credits_per_dpt[course.department] += course.credits
    end
    credits_per_dpt
  end

end
